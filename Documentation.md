# TapNation Remote Config

## Integration Steps

1) **"Install"** or **"Upload"** FG TNRemoteConfig plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

3) To finish your integration, follow the **Account and Settings** section.

## Account and Settings

To use TapNation RemoteConfig you will need to have your app and account created on TapNation's Studio Platform dashboard.

## Create an A/B test

To submit an A/B test or a CONFIG on the TapNation platform, you must select your app on the "A/B Test" section and click the "Submit A/B Test" button.

You will then need to create the A/B test by filling the following informations:

 - The type (ABTEST or CONFIG).
 - Your App Bundle Id
 - The platforms you want to apply the config for (Android, iOS or both)
 - The minimum build version you want to apply the A/B test for (you can find your current build version in the Unity Player Settings)
 - The device Id (optional). This field must be filled only for testing purposes if you want to try your config on a specific device.
 - The starting date and duration of the A/B Test (doesn't apply for CONFIGs)
 - The cohort setup.
 
 ![](_source/tnRemoteConfig_createABtest.png)
 
 To setup your cohort, you need to define the % of user that will be affected by the remote config, and the values of remote variables that will change within the game.
 
  ![](_source/tnRemoteConfig_setupCohort.png)

Once created, the A/B Test status will be "Pending". Ask your Publishing Manager to approve it in order to start the A/B test campaign. 

## Test Mode
  
Since FGRemoteConfig 3.2.4, we automatically detect the build environment of the app and set the device ID sent to our API accordingly. In case of a "Test environment", "1111-1111-1111-1111" will allways be sent as a default Device ID. This will enable you to easily test different cohorts on your test device, by creating specific CONFIG with this test ID.

These are the conditions we use to detect a Test environment from your app:

### Android

The app must be built as .APK file.

### IOS

The app must be built as Ad Hoc Distribution from XCode, and the .IPA installed manually in the testing device (ex: using Apple Configurator).

### Editor

Allways set as "Test Environment"